package com.bupt.mtl.io;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import com.bupt.mtl.graph.ListTrans;

/**
 * Construct transaction bases for vertices.
 * An itemset is a set of integers
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class ListTransInputFormat{
	//the filepath of transactions
	private String transPath = "";
	//the filepath of items
	private String itemsPath = "";
	private ArrayList<Integer> trans = new ArrayList<Integer>();
	private ArrayList<ArrayList<Integer>> transBases = new ArrayList<ArrayList<Integer>>();
	private ListTrans listTrans;
	private HashMap<Integer, Integer> vertexTrans = new HashMap<Integer, Integer>();

	public ListTransInputFormat() {
		listTrans = new ListTrans(trans, transBases, vertexTrans);
	}
	public HashMap<Integer, Integer> getTransMap() {
		return vertexTrans;
	}
	public String getTranspath() {
		return transPath;
	}
	public void setTranspath(String inputpath) {
		this.transPath = inputpath;
	}
	public String getItemspath() {
		return itemsPath;
	}
	public void setItemspath(String inputpath) {
		this.itemsPath = inputpath;
	}
	public ArrayList<ArrayList<Integer>> getTransbase() {
		return this.transBases;
	}

	//load transactions
	public static Object[] reader(String inputPath) throws Exception {
		List<String> list = new ArrayList<String>();
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(inputPath);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				list.add(line);
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
		Object[] nums = list.toArray();
		return nums;
	}

	//initial transaction datebases for vertices
	public ArrayList<ArrayList<Integer>> transInit() throws Exception {
		Object[] vertexToTrans = reader(transPath);
		Object[] transToItems = reader(itemsPath);
		for (int k = 0; k < vertexToTrans.length; k++) {
			String oneEdge[] = vertexToTrans[k].toString().split("\t");
			ArrayList<Integer> oneTrans = new ArrayList<Integer>();
			Integer vertex = Integer.valueOf(oneEdge[0].toString());
			if (!vertexTrans.containsKey(vertex)) {
				vertexTrans.put(Integer.valueOf(oneEdge[0].toString()), transBases.size());
				if (vertexTrans.get(vertex).intValue() == transBases.size()) {
					String tranStr = transToItems[Integer.valueOf(oneEdge[1].toString()) - 1].toString();
					if (!tranStr.equals("")) {
						oneTrans.add(Integer.valueOf(oneEdge[1].toString()) - 1);
						this.listTrans.add(oneTrans);
					} else {
						this.listTrans.add(oneTrans);
					}
				}
			} else {
				if (transBases.get(vertexTrans.get(vertex)) != null) {
					oneTrans = transBases.get(vertexTrans.get(vertex));
					String tranStr = transToItems[Integer.valueOf(oneEdge[1].toString()) - 1].toString();
					if (!tranStr.equals("")) {
						oneTrans.add(Integer.valueOf(oneEdge[1].toString()) - 1);
						this.listTrans.set(vertexTrans.get(vertex).intValue(), oneTrans);
					}
				}
			}
		}
		return transBases;
	}
}
