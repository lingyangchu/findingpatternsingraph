package com.bupt.mtl.io;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import com.bupt.mtl.graph.IntegerListGraph;

/**
 * Construct a graph.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class IntegerListGraphInputFormat{
	//the file for graph
	private String path = "";
	//edge set
	private ArrayList<ArrayList<Integer>> edgeSet = new ArrayList<ArrayList<Integer>>();
	// a mapping from vertex-id to the position of its edges
	private HashMap<Integer, Integer> vertexMap = new HashMap<Integer, Integer>();
	// degrees
	private HashMap<Integer, Integer> outDegree = new HashMap<Integer, Integer>();
	private IntegerListGraph directedGraph;

	public IntegerListGraphInputFormat() {
		directedGraph = new IntegerListGraph(vertexMap, edgeSet, outDegree);
	}
	public String getPath() {
		return path;
	}
	public HashMap<Integer, Integer> getDegree() {
		return outDegree;
	}
	public HashMap<Integer, Integer> getVertexMap() {
		return vertexMap;
	}
	public void setPath(String inputpath) {
		this.path = inputpath;
	}
	// read graph file
	public Object[] reader() throws Exception {
		List<String> list = new ArrayList<String>();
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(path);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				list.add(line);
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
		Object[] nums = list.toArray();
		return nums;
	}

	// initial graph structure
	public ArrayList<ArrayList<Integer>> graphInit() throws Exception {
		Object[] graphToEdges = reader();
		for (int k = 0; k < graphToEdges.length; k++) {
			String oneEdge[] = graphToEdges[k].toString().split("\t");
			ArrayList<Integer> edges = new ArrayList<Integer>();
			Integer vertex = Integer.valueOf(oneEdge[0].toString());
			if (!vertexMap.containsKey(vertex)) {
				vertexMap.put(Integer.valueOf(oneEdge[0].toString()), edgeSet.size());
				if (vertexMap.get(vertex).intValue() == edgeSet.size()) {
					edges.add(Integer.valueOf(oneEdge[1].toString()));
					this.directedGraph.initialize(vertex, edges);
				} else {
					edges = edgeSet.get(vertexMap.get(vertex));
					if (!edges.contains(Integer.valueOf(oneEdge[1].toString()))) {
						edges.add(Integer.valueOf(oneEdge[1].toString()));
					}
					this.directedGraph.initialize(vertex, edges);
				}
			} else {
				edges = edgeSet.get(vertexMap.get(vertex));
				if (!edges.contains(Integer.valueOf(oneEdge[1].toString()))) {
					edges.add(Integer.valueOf(oneEdge[1].toString()));
				}
				this.directedGraph.initialize(vertex, edges);
			}
			this.directedGraph.setDegree(vertex);
		}
		return edgeSet;
	}
}
