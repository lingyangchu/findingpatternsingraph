package com.bupt.mtl.io;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import com.bupt.mtl.graph.StringTrans;

/**
 * Construct transaction bases for vertices.
 * An itemset is a set of strings
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class StringTransInputFormat{
	//the filepath of transactions
	private String transPath = "";
	//the filepath of items
	private String itemsPath = "";
	private ArrayList<String> trans = new ArrayList<String>();
	private ArrayList<ArrayList<String>> transBases = new ArrayList<ArrayList<String>>();
	private StringTrans stringTrans;
	private HashMap<Integer, Integer> vertexTrans = new HashMap<Integer, Integer>();
	public StringTransInputFormat() {
		stringTrans = new StringTrans(trans, transBases, vertexTrans);
	}
	public HashMap<Integer, Integer> getTransMap() {
		return vertexTrans;
	}
	public String getTranspath() {
		return transPath;
	}
	public void setTranspath(String inputpath) {
		this.transPath = inputpath;
	}
	public String getItemspath() {
		return itemsPath;
	}
	public void setItemspath(String inputpath) {
		this.itemsPath = inputpath;
	}
	public ArrayList<ArrayList<String>> getTransbase() {
		return this.transBases;
	}
	///load transactions
	public static Object[] reader(String inputPath) throws Exception {
		List<String> list = new ArrayList<String>();
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(inputPath);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				list.add(line);
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
		Object[] nums = list.toArray();
		return nums;
	}

	//initial transaction datebases for vertices
	public ArrayList<ArrayList<String>> transInit() throws Exception {
		Object[] vertexToTrans = reader(transPath);
		Object[] transToItems = reader(itemsPath);
		for (int k = 0; k < vertexToTrans.length; k++) {
			String oneEdge[] = vertexToTrans[k].toString().split("\t");
			ArrayList<String> oneTrans = new ArrayList<String>();
			Integer vertex = Integer.valueOf(oneEdge[0].toString());
			if (!vertexTrans.containsKey(vertex)) {
				vertexTrans.put(Integer.valueOf(oneEdge[0].toString()), transBases.size());
				if (vertexTrans.get(vertex).intValue() == transBases.size()) {
					String tranStr = transToItems[Integer.valueOf(oneEdge[1].toString()) - 1].toString();
					if (!tranStr.equals("")) {
						oneTrans.add(tranStr);
						this.stringTrans.add(oneTrans);
					} else {
						this.stringTrans.add(oneTrans);
					}

				}
			} else {
				if (transBases.get(vertexTrans.get(vertex)) != null) {
					oneTrans = transBases.get(vertexTrans.get(vertex));
					String tranStr = transToItems[Integer.valueOf(oneEdge[1].toString()) - 1].toString();
					if (!tranStr.equals("")) {
						oneTrans.add(tranStr);
						this.stringTrans.set(vertexTrans.get(vertex).intValue(), oneTrans);
					}
				}
			}
		}
		return transBases;
	}
}
