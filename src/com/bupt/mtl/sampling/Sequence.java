package com.bupt.mtl.sampling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ArrayList;

/**
 * A class fo sequences.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class Sequence {

	public String seqString = "";
	public int weight = 0;

	public String getSeqString() {
		return seqString;
	}

	public void setSeqString(String seqString) {
		this.seqString = seqString;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
