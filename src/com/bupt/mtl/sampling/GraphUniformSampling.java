package com.bupt.mtl.sampling;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.bupt.mtl.graph.IntegerListGraph;
import com.bupt.mtl.graph.ListTrans;
import com.bupt.mtl.io.IntegerListGraphInputFormat;
import com.bupt.mtl.io.StringTransInputFormat;

/**
 * The uniform sampling method for mining sequential patterns in the graph.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class GraphUniformSampling {
	private Integer vertex;
	//edges
	private ArrayList<ArrayList<Integer>> edgeSet = new ArrayList<ArrayList<Integer>>();
	//degree for one vertex
	private HashMap<Integer, Integer> outDegree = new HashMap<Integer, Integer>();
	//path size
	private int pathSize = 0;
	//sample size
	private int sampleSize = 0;
	//vertices map to edges
	private HashMap<Integer, Integer> vertexMap = new HashMap<Integer, Integer>();
	//vertices map to transactions
	private HashMap<Integer, Integer> vertexTrans = new HashMap<Integer, Integer>();
	//transaction bases for vertices
	private ArrayList<ArrayList<String>> transBases = new ArrayList<ArrayList<String>>();
	//vertices map to degrees
	private HashMap<Integer, Integer> vertexDegree = new HashMap<Integer, Integer>();
	//degrees for every vertex
	private ArrayList<ArrayList<Integer>> multiDegrees = new ArrayList<ArrayList<Integer>>();
	//the file path of graph
	private String graphPath = "";
	//the file path of items
	private String itemsPath = "";
	//the file path of transactions
	private String transPath = "";
	//the output file path of sampled sequences
	private String outPutPath = "";
	private IntegerListGraphInputFormat graphInput;
	private StringTransInputFormat transInput;
	private ArrayList<Sequence> sampleBase = new ArrayList<Sequence>();
	private HashMap<String, Integer> samplePaths = new HashMap<String, Integer>();
	private String sampledPath = "";
	private Random r = new Random();
	private String pathOutPath = "";
	private ArrayList<String> pathBase = new ArrayList<String>();

	public GraphUniformSampling(IntegerListGraphInputFormat graphInput, StringTransInputFormat transInput) {
		this.graphInput = graphInput;
		this.transInput = transInput;
	}

	//initial graph and transactions
	public void init() throws Exception {
		graphInput.setPath(graphPath);
		transInput.setItemspath(itemsPath);
		transInput.setTranspath(transPath);
		edgeSet = graphInput.graphInit();
		transBases = transInput.transInit();
		outDegree = graphInput.getDegree();
		vertexMap = graphInput.getVertexMap();
		vertexTrans = transInput.getTransMap();
	}
	public ArrayList<ArrayList<Integer>> getGraph() {
		return this.edgeSet;
	}
	public ArrayList<ArrayList<String>> getTransbase() {
		return this.transBases;
	}
	public ArrayList<Sequence> getSamplebase() {
		return this.sampleBase;
	}
	public HashMap<Integer, Integer> getVertexMap() {
		return this.vertexMap;
	}
	public HashMap<Integer, Integer> getOutdegree() {
		return this.outDegree;
	}
	public void setMultidegree(ArrayList<ArrayList<Integer>> multidegree) {
		this.multiDegrees = multidegree;
	}
	public HashMap<String, Integer> getSamplepaths() {
		return samplePaths;
	}
	public void setSamplepaths(HashMap<String, Integer> samplepaths) {
		this.samplePaths = samplepaths;
	}
	public String getSampledpath() {
		return sampledPath;
	}
	public void setSampledpath(String sampledpath) {
		this.sampledPath = sampledpath;
	}
	public int getPathSize() {
		return pathSize;
	}
	public void setPathSize(int pathsize) {
		this.pathSize = pathsize;
	}
	public int getSampleSize() {
		return sampleSize;
	}
	public void setSampleSize(int samplesize) {
		this.sampleSize = samplesize;
	}
	public HashMap<Integer, Integer> getVertexTrans() {
		return this.vertexTrans;
	}
	public void setGraphPath(String GraphPath) {
		this.graphPath = GraphPath;
	}
	public String getGraphPath() {
		return graphPath;
	}
	public void setTransPath(String transpath) {
		this.transPath = transpath;
	}
	public String getTransPath() {
		return transPath;
	}
	public void setItemsPath(String itemspath) {
		this.itemsPath = itemspath;
	}
	public String getItemsPath() {
		return itemsPath;
	}
	public void setOutputPath(String outputpath) {
		this.outPutPath = outputpath;
	}
	public String getOutputPath() {
		return outPutPath;
	}
	public Integer getVertex() {
		return vertex;
	}
	public void setVertex(Integer vertex) {
		this.vertex = vertex;
	}
	public String getPathoutpath() {
		return pathOutPath;
	}
	public void setPathoutpath(String pathoutpath) {
		this.pathOutPath = pathoutpath;
	}
	public ArrayList<String> getPathbase() {
		return pathBase;
	}
	public void setPathbase(ArrayList<String> pathbase) {
		this.pathBase = pathbase;
	}
	/** obtain outdegrees in each jump for vertices */
	public ArrayList<ArrayList<Integer>> getMultidegrees() {
		ArrayList<ArrayList<Integer>> multiDegree = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i <= this.pathSize; i++) {
			if (i == 0) {
				for (Integer key : this.vertexTrans.keySet()) {
					ArrayList<Integer> degree = new ArrayList<Integer>();
					degree.add(1);
					multiDegree.add(degree);
					this.vertexDegree.put(key, multiDegree.size() - 1);
				}
			} else if (i == 1) {
				for (int key : this.vertexDegree.keySet()) {
					ArrayList<Integer> degree = new ArrayList<Integer>();
					degree = multiDegree.get(this.vertexDegree.get(key));
					if (outDegree.containsKey(key)) {
						degree.add(outDegree.get(key));
						multiDegree.set(this.vertexDegree.get(key), degree);
					} else {
						degree.add(0);
						multiDegree.set(this.vertexDegree.get(key), degree);
					}
				}
			} else {
				for (int key : this.vertexDegree.keySet()) {
					ArrayList<Integer> degree = new ArrayList<Integer>();
					degree = multiDegree.get(this.vertexDegree.get(key));
					int lastDegree = 0;
					if (vertexMap.containsKey(key)) {
						ArrayList<Integer> neighbours = new ArrayList<Integer>();
						neighbours = edgeSet.get(vertexMap.get(key));
						for (int j = 0; j < neighbours.size(); j++) {
							Integer neighbour = neighbours.get(j);
							lastDegree = lastDegree + multiDegree.get(this.vertexDegree.get(neighbour)).get(i - 1);
						}
						degree.add(lastDegree);
						multiDegree.set(this.vertexDegree.get(key), degree);
					} else {
						degree.add(lastDegree);
						multiDegree.set(this.vertexDegree.get(key), degree);
					}

				}
			}
		}
		return multiDegree;
	}

	/** sample the first vertex in the graph */
	public Integer getRandomFirstVertex() {
		Integer vertex = 0;
		HashMap<Integer, Integer> rand = new HashMap<Integer, Integer>();
		int sum = 0;
		ArrayList<Integer> degreeList = new ArrayList<Integer>();
		for (Integer key : vertexDegree.keySet()) {
			int degreeLocation = this.vertexDegree.get(key).intValue();
			degreeList = this.multiDegrees.get(degreeLocation);
			if (degreeList.get(this.pathSize) != 0) {
				sum = sum + degreeList.get(this.pathSize);
				rand.put(key, sum);
			}
		}
		if (sum != 0) {
			int num = r.nextInt(sum) + 1;
			int lastNum = 0;
			int nextNum = 0;
			int lastVertex = 0;
			int nextVertex = 0;
			for (int key : rand.keySet()) {
				nextNum = rand.get(key);
				nextVertex = key;
				if (num >= lastNum && num <= nextNum) {
					if (num == lastNum) {
						vertex = lastVertex;
					} else {
						vertex = nextVertex;
					}
					break;
				} else {
					lastNum = rand.get(key);
					lastVertex = key;
				}
			}
		}
		return vertex;
	}

	/** sample the next vertex based on last vertex */
	public Integer getRandomNextVertex(Integer vertexid, int pathNum) {
		Integer vertex = 0;
		ArrayList<Integer> edges = new ArrayList<Integer>();
		if (this.vertexMap.get(vertexid) != null) {
			edges = this.edgeSet.get(this.vertexMap.get(vertexid).intValue());
			Iterator<Integer> edgeIter = edges.iterator();
			HashMap<Integer, Integer> rand = new HashMap<Integer, Integer>();
			int sum = 0;
			if (pathNum == this.pathSize) {
				Object[] array = edges.toArray();
				int x = r.nextInt(edges.size());
				vertex = Integer.valueOf(array[x].toString());
			} else {
				while (edgeIter.hasNext()) {
					Integer neighbor = edgeIter.next();
					int degreeLocation = this.vertexDegree.get(neighbor).intValue();
					int degreeNum = this.pathSize - pathNum;
					ArrayList<Integer> degreeList = this.multiDegrees.get(degreeLocation);
					if (degreeList.get(degreeNum) != 0) {
						sum = sum + degreeList.get(degreeNum);
						rand.put(neighbor, sum);
					}
				}
				if (sum != 0) {
					int num = r.nextInt(sum) + 1;
					int lastNum = 0;
					int nextNum = 0;
					int lastVertex = 0;
					int nextVertex = 0;
					for (int key : rand.keySet()) {
						nextNum = rand.get(key);
						nextVertex = key;
						if (num >= lastNum && num <= nextNum) {
							if (num == lastNum) {
								vertex = lastVertex;
							} else {
								vertex = nextVertex;
							}
							break;
						} else {
							lastNum = rand.get(key);
							lastVertex = key;
						}
					}
				}
			}
		}
		return vertex;
	}

	/** sample a transaction for a vertex */
	public Sequence getRandomTrans(Integer vertexId) {
		String trans = "";
		ArrayList<String> transList = new ArrayList<String>();
		Sequence seq = new Sequence();
		if (this.vertexTrans.get(vertexId) != null) {
			transList = this.transBases.get(this.vertexTrans.get(vertexId).intValue());
			if (transList.size() > 0) {
				int num = r.nextInt(transList.size());
				trans = transList.get(num);
				seq.setSeqString(trans);
				seq.setWeight(transList.size());
			}
		} else {
			seq.setSeqString("");
			seq.setWeight(0);
		}

		return seq;
	}

	/** get sampled sequence for sampled path **/
	public void getSampleSequence() {
		if (vertex != 0) {
			String onePath = "";
			onePath = onePath + this.vertex + "\t";
			Sequence seq = new Sequence();
			Sequence firstSeq = this.getRandomTrans(vertex);
			seq.setSeqString(firstSeq.getSeqString());
			seq.setWeight(firstSeq.getWeight());
			int pathNum = 0;
			if (this.pathSize != 0) {
				Integer nextVertex = vertex;
				for (int j = 1; j <= pathSize; j++) {
					pathNum = pathNum + 1;
					nextVertex = this.getRandomNextVertex(nextVertex, j);
					Sequence nextSeq = this.getRandomTrans(nextVertex);
					seq.setSeqString(seq.getSeqString() + "\t" + "-1" + "\t" + nextSeq.getSeqString());
					seq.setWeight(seq.getWeight() * nextSeq.getWeight());
					onePath = onePath + nextVertex + "\t";
				}
				if (pathNum == this.pathSize) {
					this.sampleBase.add(seq);
					onePath = onePath.substring(0, onePath.length() - 1);
					pathBase.add(onePath);
					if (samplePaths.containsKey(onePath)) {
						samplePaths.put(onePath, samplePaths.get(onePath) + 1);
					} else {
						samplePaths.put(onePath, 1);
					}
				}
			}
		}
	}
	
	/** write paths to file */
	public void pathsWriter() throws Exception {
		FileOutputStream out = new FileOutputStream(this.pathOutPath);
		OutputStreamWriter osw = new OutputStreamWriter(out, "UTF8");
		BufferedWriter bwWrite = new BufferedWriter(osw);
		if (this.pathBase.size() != 0) {
			for (int i = 0; i < this.pathBase.size(); i++) {
				String onePath = "";
				onePath = this.pathBase.get(i);
				bwWrite.write(onePath);
				bwWrite.newLine();
			}
		}
		bwWrite.close();
		osw.close();
		out.close();
	}
	/** write sequences to file */
	public void sequneceWriter() throws Exception {
		FileOutputStream out = new FileOutputStream(this.outPutPath);
		OutputStreamWriter osw = new OutputStreamWriter(out, "UTF8");
		BufferedWriter bwWrite = new BufferedWriter(osw);
		if (this.sampleBase.size() != 0) {
			for (int i = 0; i < this.sampleBase.size(); i++) {
				Sequence seq = new Sequence();
				seq = sampleBase.get(i);
				bwWrite.write(seq.getSeqString() + " " + seq.getWeight());
				bwWrite.newLine();
			}
		}
		bwWrite.close();
		osw.close();
		out.close();
	}

}
