package com.bupt.mtl.graph;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A kind of format for transactions. An itemset is a set of integers
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class ListTrans{

	private ArrayList<Integer> trans = new ArrayList<Integer>();
	//transaction bases
	private ArrayList<ArrayList<Integer>> transBase = new ArrayList<ArrayList<Integer>>();
	//one vertex has a transactions
	private HashMap<Integer, Integer> vertexTrans = new HashMap<Integer, Integer>();

	public ListTrans(ArrayList<Integer> Trans, ArrayList<ArrayList<Integer>> Transbase,
			HashMap<Integer, Integer> Vertextrans) {
		this.trans = Trans;
		this.transBase = Transbase;
		this.vertexTrans = Vertextrans;
	}
	public ListTrans() {
		// TODO Auto-generated constructor stub
	}
	public ArrayList<ArrayList<Integer>> getTrans() {
		return transBase;
	}
	public void add(ArrayList<Integer> trans) {
		transBase.add(trans);
	}
	public void set(int index, ArrayList<Integer> trans) {
		transBase.set(index, trans);
	}
}
