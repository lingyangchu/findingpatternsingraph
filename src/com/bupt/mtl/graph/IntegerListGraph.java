package com.bupt.mtl.graph;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * One format of graph. 
 * The format of vertices is HashMap<Integer, Integer>.
 * The format of edges is ArrayList<ArrayList<Integer>>.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class IntegerListGraph {
	//edge set
	private ArrayList<ArrayList<Integer>> edgeSet;
	//vertex map to edges
	private HashMap<Integer, Integer> vertexMap;
	//degrees
	private HashMap<Integer, Integer> outDegree;

	public IntegerListGraph(HashMap<Integer, Integer> vertexMap, ArrayList<ArrayList<Integer>> edgeSet,
			HashMap<Integer, Integer> outDegree) {
		this.vertexMap = vertexMap;
		this.edgeSet = edgeSet;
		this.outDegree = outDegree;
	}

	// 鍒濆鍖栧浘缁撴瀯
	public void initialize(Integer vertex, ArrayList<Integer> edges) {
		if (vertexMap.get(vertex).intValue() >= edgeSet.size()) {
			edgeSet.add(edges);
		} else {
			edgeSet.set(vertexMap.get(vertex).intValue(), edges);
		}
	}
	public ArrayList<ArrayList<Integer>> getGraph() {
		return edgeSet;
	}
	public HashMap<Integer, Integer> getVertexMap() {
		return vertexMap;
	}
	public ArrayList<Integer> getEdges(Integer vertex) {
		return edgeSet.get(vertexMap.get(vertex).intValue());
	}
	public void setEdges(Integer vertex, ArrayList<Integer> edges) {
		edgeSet.set(vertexMap.get(vertex).intValue(), edges);
		;
	}
	public void setVertexMap(Integer vertex, Integer index) {
		vertexMap.put(vertex, index);
	}
	public void setDegree(Integer vertex) {
		outDegree.put(vertex, edgeSet.get(vertexMap.get(vertex).intValue()).size());
	}
	public Integer getDegree(Integer vertex) {
		return outDegree.get(vertex);
	}
	public int getNumEdges(Integer vertex) {
		return edgeSet.get(vertexMap.get(vertex).intValue()).size();
	}
}
