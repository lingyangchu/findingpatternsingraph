package com.bupt.mtl.graph;

import java.util.HashSet;

/**
 * edges with integer format for every vertex.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class IntegerEdge {
	//ids for target
	private HashSet<Integer> edges = new HashSet<Integer>();
	public HashSet<Integer> getTargetVertex() {
		return edges;
	}
	// if an edge doesn't exist, store it
	public void add(Integer targetVertexId) {
		if (!edges.contains(targetVertexId)) {
			edges.add(targetVertexId);
		}
	}
	// if an edge exists, remove it
	public void remove(Integer targetVertexId) {
		if (edges.contains(targetVertexId)) {
			edges.remove(targetVertexId);
		}
	}
	// get the number of edges
	public int size() {
		return edges.size();
	}
}
