package com.bupt.mtl.graph;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * a kind of format for transactions. An itemset is a set of Strings.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class StringTrans{

	private ArrayList<String> trans = new ArrayList<String>();
	//transaction bases
	private ArrayList<ArrayList<String>> transBase = new ArrayList<ArrayList<String>>();
	//one vertex has a transactions
	private HashMap<Integer, Integer> vertexTrans = new HashMap<Integer, Integer>();
	
	public StringTrans(ArrayList<String> Trans, ArrayList<ArrayList<String>> Transbase,
			HashMap<Integer, Integer> Vertextrans) {
		this.trans = Trans;
		this.transBase = Transbase;
		this.vertexTrans = Vertextrans;
	}
	public StringTrans() {
		// TODO Auto-generated constructor stub
	}
	public ArrayList<ArrayList<String>> getTrans() {
		return transBase;
	}
	public void add(ArrayList<String> trans) {
		transBase.add(trans);
	}
	public void set(int index, ArrayList<String> trans) {
		transBase.set(index, trans);
	}
}
