package com.bupt.mtl.mining;
import java.util.HashSet;

/**
 * Check a sequence contains which patterns.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class Combination {
	//unique patterns
	private HashSet<String> comResult = new HashSet<String>();
	//get patterns
	public HashSet<String> getComResult() {
		return comResult;
	}
	//set patterns
	public void setComResult(HashSet<String> comResult) {
		this.comResult = comResult;
	}
	/**
	 * make a combination.
	 * @param 
	 * @return void
	 */
	public void combination(String[] pattern, int n) {
		combination("", pattern, n);
	}
	/**
	 * make a combination.
	 * @param 
	 * @return void
	 */
	public void combination(String source, String[] pattern, int n) {
		if(n==1){
			for(int i=0;i<pattern.length;i++) {
				comResult.add(source + pattern[i]);
			}
		}else{
			for(int i=0; i<pattern.length-(n-1);i++) {
				String target="";
				target=source+pattern[i]+"\t";
				String[] temp=new String[pattern.length-i-1];
				for(int j=0;j<pattern.length-i-1;j++) {
					temp[j]=pattern[i+j+1];
				}
				combination(target,temp,n-1);
			}
		}
	}

}
