package com.bupt.mtl.mining;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import com.bupt.mtl.io.IntegerListGraphInputFormat;
import com.bupt.mtl.io.StringTransInputFormat;
import com.bupt.mtl.patterns.PatternHeap;
import com.bupt.mtl.patterns.WeightedSequenceMining;
import com.bupt.mtl.sampling.GraphUniformSampling;
import com.bupt.mtl.sampling.Sequence;

/**
 * Mine top-k sequential patterns by using our sampling method.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class MineSequenceWithSampling {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		System.out.println("Running MineSequenceWithSampling demo (wait for a minute) ... ");
		
		//set sample size and the minimal support
		HashMap<Integer, Integer> sampleConf = new HashMap<Integer, Integer>();
		//set the different values of k
		ArrayList<Integer> kConf = new ArrayList<Integer>();
		sampleConf.put(10000, 50);
		int k=5;
		for (Integer sampleSize : sampleConf.keySet()) {
			IntegerListGraphInputFormat graphInput = new IntegerListGraphInputFormat();
			StringTransInputFormat transInput = new StringTransInputFormat();
			ArrayList<ArrayList<Integer>> multiDegree = new ArrayList<ArrayList<Integer>>();
			ArrayList<Sequence> sampleBase = new ArrayList<Sequence>();
			WeightedSequenceMining samplePattern = new WeightedSequenceMining();
			HashMap<String, Integer> sampleAll = new HashMap<String, Integer>();
			HashMap<String, Integer> itemTransfer = new HashMap<String, Integer>();
			HashSet<String> globalItems = new HashSet<String>();
			HashMap<Integer, String> integerItem = new HashMap<Integer, String>();
			File graphFile=new File("demo/graph.txt");
			File transFile=new File("demo/trans.txt");
			File itemsFile=new File("demo/items.txt");
			File outputFile=new File("demo/result/patterns.txt");
			//the filepath of storing graph
			String graphPath = graphFile.getAbsolutePath();
			//the filepath of storing transactions for every vertex
			String transPath = transFile.getAbsolutePath();
			//the filepath of storing itemsets
			String itemsPath = itemsFile.getAbsolutePath();
			//set output path for patterns
			String frePaPath = outputFile.getAbsolutePath();
			/** load configuration */			
			int pathSize = 1;
			int min_sup = sampleConf.get(sampleSize);
			GraphUniformSampling sample = new GraphUniformSampling(graphInput, transInput);
			sample.setGraphPath(graphPath);
			sample.setTransPath(transPath);
			sample.setItemsPath(itemsPath);
			sample.init();
			sample.setPathSize(pathSize);
			sample.setSampleSize(sampleSize);
			multiDegree = sample.getMultidegrees();
			sample.setMultidegree(multiDegree);
			/** sampling */
			Integer vertex;
			for (int i = 0; i < sampleSize; i++) {
				vertex = sample.getRandomFirstVertex();
				sample.setVertex(vertex);
				sample.getSampleSequence();
			}
			sampleBase = sample.getSamplebase();
			/** mining */
			samplePattern.setSequenceBase(sampleBase);
			samplePattern.setK(k);
			samplePattern.setMin_sup(min_sup);
			samplePattern.setPathsize(pathSize);
			samplePattern.getFrequentSingles();
			globalItems = samplePattern.getGlobalItems();
			
			for (String item : globalItems) {
				itemTransfer.put(item, itemTransfer.size() + 1);
			}
			//give every item an unique id
			samplePattern.setItemTransfer(itemTransfer);
			samplePattern.prefixSpanCalculate();
			sampleAll = samplePattern.getFrePatterns();
			for (String item : itemTransfer.keySet()) {
				integerItem.put(itemTransfer.get(item), item);
			}
			HashMap<String, Integer> newPattern = new HashMap<String, Integer>();
			for (String patternStr : sampleAll.keySet()) {
				String pString = "";
				String[] items = patternStr.split("\t");
				for (int j = 0; j < items.length; j++) {
					if (items[j].equals("-1")) {
						pString = pString + items[j] + "\t";
					} else {
						pString = pString + integerItem.get(Integer.valueOf(items[j])) + "\t";
					}
				}
				newPattern.put(pString.substring(0, pString.length() - 1), sampleAll.get(patternStr));
			}
			/** select top-k patterns */
			HashMap<String, Integer> sampleTopK = new HashMap<String, Integer>();
			PatternHeap sampleHeap = new PatternHeap(k);
			sampleHeap.topKPatterns(newPattern);
			sampleTopK = sampleHeap.topkMap(sampleHeap.queue);
			/** output patterns */			
			samplePattern.setFrePaPath(frePaPath);
			samplePattern.setFrePatterns(sampleTopK);
			samplePattern.patternWriter();			
		}
		
		System.out.println("Done! Please check the result 'patterns.txt'");
	}
}