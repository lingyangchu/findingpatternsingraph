package com.bupt.mtl.mining;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/**
 * count real support for patterns.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class VerifyInSequence {
	// all transcations
	private ArrayList<String> sequenceBase = new ArrayList<String>();
	// frequent patterns
	private HashMap<String, Integer> frePatterns = new HashMap<String, Integer>();
	// single items
	private ArrayList<HashSet<String>> singleItems = new ArrayList<HashSet<String>>();
	// read file from filepath
	private String filePath = "";

	private HashMap<String, Integer> patternCount = new HashMap<String, Integer>();

	public ArrayList<String> getSequenceBase() {
		return sequenceBase;
	}

	public void setSequenceBase(ArrayList<String> sequenceBase) {
		this.sequenceBase = sequenceBase;
	}

	public HashMap<String, Integer> getFrePatterns() {
		return frePatterns;
	}

	public void setFrePatterns(HashMap<String, Integer> frePatterns) {
		this.frePatterns = frePatterns;
	}

	public ArrayList<HashSet<String>> getSingleItems() {
		return singleItems;
	}

	public void setSingleItems(ArrayList<HashSet<String>> singleItems) {
		this.singleItems = singleItems;
	}

	public String getFilepath() {
		return filePath;
	}

	public void setFilepath(String filepath) {
		this.filePath = filepath;
	}

	public HashMap<String, Integer> getPatternCount() {
		return patternCount;
	}

	public void setPatternCount(HashMap<String, Integer> patternCount) {
		this.patternCount = patternCount;
	}

	/** Read transactions from files */
	public void sequenceReader() throws Exception {
		FileInputStream inputstream = null;
		Scanner sc = null;
		try {
			inputstream = new FileInputStream(filePath);
			sc = new Scanner(inputstream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				sequenceBase.add(line);
			}
		} finally {
			if (inputstream != null) {
				inputstream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
	}

	/** enumerate single items */
	public void getSingles() {
		int i = 0;
		for (String pattern : frePatterns.keySet()) {
			String[] singlePas = null;
			singlePas = pattern.split("\t-1\t");

			for (int j = 0; j < singlePas.length; j++) {
				String[] items = null;
				items = singlePas[j].split("\t");
				HashSet<String> tempItems = new HashSet<String>();
				if (i == 0) {
					for (int m = 0; m < items.length; m++) {
						if (!tempItems.contains(items[m])) {
							tempItems.add(items[m]);
						}
					}
					singleItems.add(tempItems);
				} else {
					tempItems = singleItems.get(j);
					for (int m = 0; m < items.length; m++) {
						if (!tempItems.contains(items[m])) {
							tempItems.add(items[m]);
						}
					}
					singleItems.set(j, tempItems);
				}
			}
			i = i + 1;
		}
	}
	/** remove infrequent items */
	public void sequenceFilter() {
		for (int i = 0; i < sequenceBase.size(); i++) {
			String[] singleTrans = null;
			String singleSeq = "";
			singleSeq = sequenceBase.get(i);
			String newSeq = "";
			singleTrans = singleSeq.split("\t-1\t");
			int flag = 0;
			for (int j = 0; j < singleTrans.length; j++) {
				HashSet<String> sItems = singleItems.get(j);
				String newTrans = "";
				String[] items = null;
				items = singleTrans[j].split("\t");
				for (int m = 0; m < items.length; m++) {
					if (sItems.contains(items[m])) {
						newTrans = newTrans + items[m] + "\t";
					}
				}
				if (newTrans.length() == 0) {
					flag = 1;
					break;
				} else {
					newTrans = newTrans.substring(0, newTrans.length() - 1);
					newSeq = newSeq + newTrans + "\t-1\t";
				}
			}
			if (flag == 1) {
				sequenceBase.remove(i);
				i = i - 1;
			} else {
				newSeq = newSeq.substring(0, newSeq.length() - 4);
				sequenceBase.set(i, newSeq);
			}
		}
	}
	/** count supports for patterns */
	public void countPatterns() {
		for (int i = 0; i < sequenceBase.size(); i++) {
			String oneSeq = sequenceBase.get(i);
			String[] singleTrans = oneSeq.split("\t-1\t");
			ArrayList<HashSet<String>> itemsCom = new ArrayList<HashSet<String>>();
			for (int j = 0; j < singleTrans.length; j++) {
				Combination com = new Combination();
				String[] items = singleTrans[j].split("\t");
				for (int k = 1; k <= items.length; k++) {
					com.combination(items, k);
				}
				HashSet<String> comResult = new HashSet<String>();
				comResult = com.getComResult();
				itemsCom.add(comResult);
			}
			for (String pattern : frePatterns.keySet()) {
				String[] subPatterns = pattern.split("\t-1\t");
				// System.out.println("11111: "+pattern);
				int flag = 0;
				for (int j = 0; j < subPatterns.length; j++) {
					String subPa = subPatterns[j];
					HashSet<String> comResult = itemsCom.get(j);
					if (!comResult.contains(subPa)) {
						flag = 1;
						break;
					}
				}
				if (flag == 0) {
					if (patternCount.containsKey(pattern)) {
						patternCount.put(pattern, patternCount.get(pattern) + 1);
					} else {
						patternCount.put(pattern, 1);
					}
				}
			}
		}
	}

}