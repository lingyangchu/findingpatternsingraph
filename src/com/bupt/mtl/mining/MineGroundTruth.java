package com.bupt.mtl.mining;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import com.bupt.mtl.groundtruth.GraphTotalIntegerTrans;
import com.bupt.mtl.io.IntegerListGraphInputFormat;
import com.bupt.mtl.io.ListTransInputFormat;
import com.bupt.mtl.patterns.FrequencyHeap;
import com.bupt.mtl.patterns.SequenceMining;

/**
 * Mine pattern for ground truth.
 * Note that only make sense in samll dataset
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class MineGroundTruth {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		//set the different values of k
		ArrayList<Integer> kConf = new ArrayList<Integer>();
		kConf.add(6);
		int pathSize = 3;
		for (int j = 0; j < kConf.size(); j++) {
			int k = kConf.get(j);
			IntegerListGraphInputFormat graphInput = new IntegerListGraphInputFormat();
			ListTransInputFormat transInput = new ListTransInputFormat();
			HashMap<Integer, Integer> vertexMap = new HashMap<Integer, Integer>();
			ArrayList<String> sequenceBase = new ArrayList<String>();
			HashMap<String, Integer> truthTopK = new HashMap<String, Integer>();
			//set minimal support
			int min_sup = 2;
			File graphFile=new File("demo/graph.txt");
			File transFile=new File("demo/trans.txt");
			File itemsFile=new File("demo/items.txt");
			File outputFile=new File("demo/result/patterns.txt");
			//the filepath of storing graph
			String graphPath = graphFile.getAbsolutePath();
			//the filepath of storing transactions for every vertex
			String transPath = transFile.getAbsolutePath();
			//the filepath of storing itemsets
			String itemsPath = itemsFile.getAbsolutePath();
			//set output path for patterns
			String frePaPath = outputFile.getAbsolutePath();
			//the filepath for real supports
			String freOutPath = "C://scratch/trans/test/truthfre_" + pathSize + k + ".txt";
			
			/** get all seqeunces */
			GraphTotalIntegerTrans totalTrans = new GraphTotalIntegerTrans(graphInput, transInput);
			totalTrans.setGraphPath(graphPath);
			totalTrans.setTransPath(transPath);
			totalTrans.setItemsPath(itemsPath);
			totalTrans.init();
			totalTrans.setPathSize(pathSize);
			vertexMap = totalTrans.getVertexMap();
			for (Integer vertex : vertexMap.keySet()) {
				totalTrans.setVertex(vertex);
				totalTrans.getAllpath();
			}
			totalTrans.getAllTrans();
			sequenceBase = totalTrans.getSequenceBase();
			totalTrans.sequenceWriter();

			/** mining */
			SequenceMining truthPattern = new SequenceMining();
			truthPattern.setFrePaPath(frePaPath);
			truthPattern.setSequenceBase(sequenceBase);
			truthPattern.setK(k);
			truthPattern.setMin_sup(min_sup);
			truthPattern.setPathsize(pathSize);
			truthPattern.getFrequentSingles();
			truthPattern.prefixSpanCalculate();
			truthTopK = truthPattern.getFrePatterns();
			truthPattern.patternWriter();
			/** output real pattern supports */
			FrequencyHeap truthFre = new FrequencyHeap(k);
			truthFre.patternWriter(freOutPath, truthTopK, k);
		}
	}
}