package com.bupt.mtl.mining;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import com.bupt.mtl.io.IntegerListGraphInputFormat;
import com.bupt.mtl.io.StringTransInputFormat;
import com.bupt.mtl.patterns.FrequencyHeap;
import com.bupt.mtl.patterns.PatternHeap;
import com.bupt.mtl.patterns.WeightedSequenceMining;
import com.bupt.mtl.sampling.GraphUniformSampling;
import com.bupt.mtl.sampling.Sequence;

/**
 * Mine sequential patterns by using our sampling method and check their supports in the graph.
 * Note that count the real support for patterns cost much since one must enumerate sequences.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

public class MiningSampleVerify {
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {
		IntegerListGraphInputFormat graphInput = new IntegerListGraphInputFormat();
		StringTransInputFormat transInput = new StringTransInputFormat();
		ArrayList<ArrayList<Integer>> multiDegree = new ArrayList<ArrayList<Integer>>();
		ArrayList<Sequence> sampleBase = new ArrayList<Sequence>();
		WeightedSequenceMining samplePattern = new WeightedSequenceMining();
		HashMap<String, Integer> sampleAll = new HashMap<String, Integer>();
		int pathSize = 1;
		File graphFile=new File("demo/graph.txt");
		File transFile=new File("demo/trans.txt");
		File itemsFile=new File("demo/items.txt");
		File outputFile=new File("demo/result/patterns.txt");
		//the filepath of storing graph
		String graphPath = graphFile.getAbsolutePath();
		//the filepath of storing transactions for every vertex
		String transPath = transFile.getAbsolutePath();
		//the filepath of storing itemsets
		String itemsPath = itemsFile.getAbsolutePath();
		//set output path for patterns
		String frePaPath = outputFile.getAbsolutePath();
		//the filepath of storing all of sequences
		String totalBasePath = "C://scratch/trans/datatruth5w/enumerate_" + pathSize + ".txt";
		//set sample size and the minimal support
		HashMap<Integer, Integer> sampleConf = new HashMap<Integer, Integer>();
		//set the different values of k
		int k=50;
		sampleConf.put(100000, 500);
		
		for (Integer sampleSize : sampleConf.keySet()) {
			int min_sup = sampleConf.get(sampleSize);
			/** sampling */
			GraphUniformSampling sample = new GraphUniformSampling(graphInput, transInput);
			sample.setGraphPath(graphPath);
			sample.setTransPath(transPath);
			sample.setItemsPath(itemsPath);
			sample.init();
			sample.setPathSize(pathSize);
			sample.setSampleSize(sampleSize);
			multiDegree = sample.getMultidegrees();
			sample.setMultidegree(multiDegree);
			Integer vertex;
			for (int i = 0; i < sampleSize; i++) {
				vertex = sample.getRandomFirstVertex();
				sample.setVertex(vertex);
				sample.getSampleSequence();
				sample.getSamplepaths();
			}
			sampleBase = sample.getSamplebase();

			/** mining */
			samplePattern.setSequenceBase(sampleBase);
			samplePattern.setK(k);
			samplePattern.setMin_sup(min_sup);
			samplePattern.setPathsize(pathSize);
			samplePattern.getFrequentSingles();
			samplePattern.prefixSpanCalculate();
			sampleAll = samplePattern.getFrePatterns();
			/** select top-k patterns*/
			HashMap<String, Integer> sampleTopK = new HashMap<String, Integer>();
			PatternHeap sampleHeap = new PatternHeap(k);
			sampleHeap.topKPatterns(sampleAll);
			sampleTopK = sampleHeap.topkMap(sampleHeap.queue);
			System.out.println("sampletop:  " + sampleTopK.size());
			//output path of patterns
			samplePattern.setFrePaPath(frePaPath);
			samplePattern.setFrePatterns(sampleTopK);
			samplePattern.patternWriter();
			/** count the real support for each pattern */
			FrequencyHeap fHeap = new FrequencyHeap(k);				
			VerifyInSequence verifyTruth = new VerifyInSequence();
			HashMap<String, Integer> patternCount = new HashMap<String, Integer>();
			verifyTruth.setFilepath(totalBasePath);
			verifyTruth.sequenceReader();
			verifyTruth.setFrePatterns(sampleTopK);
			verifyTruth.getSingles();
			verifyTruth.sequenceFilter();
			verifyTruth.countPatterns();
			patternCount = verifyTruth.getPatternCount();
			//output the real frequencies in an order of descending
			fHeap.patternWriter("C://scratch/trans/datatruth5w/samplefre/sampledfre_" + pathSize + "_" + sampleSize
						+ "_" + k + ".txt", patternCount, k);				
		}
	}
}