package com.bupt.mtl.groundtruth;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;

import com.bupt.mtl.graph.IntegerListGraph;
import com.bupt.mtl.io.IntegerListGraphInputFormat;
import com.bupt.mtl.io.ListTransInputFormat;

/**
 * Check a sequence contains how many patterns.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class GraphTotalIntegerTrans {
	private Integer vertex;
	// graph，每一行存储每点的边
	private ArrayList<ArrayList<Integer>> edgeSet = new ArrayList<ArrayList<Integer>>();
	// 点与出度的映射表，key是点，value是出度
	private HashMap<Integer, Integer> outDegree = new HashMap<Integer, Integer>();
	// path的长度
	private int pathSize = 0;
	// 点与图的映射表，key表示点id，value表示该点的边的位置
	private HashMap<Integer, Integer> vertexMap = new HashMap<Integer, Integer>();
	// 点与trans的映射表，key表示点id，value表示点的trans存储的位置
	private HashMap<Integer, Integer> vertexTrans = new HashMap<Integer, Integer>();
	// 图中所有点的所有trans
	private ArrayList<ArrayList<Integer>> transBase = new ArrayList<ArrayList<Integer>>();
	// 图的存储路径，每一行表示一条边
	private String graphPath = "";
	// item的存储路径，每一行表示若干item
	private String itemsPath = "";
	// trans的存储路径，每一行表示某点具有某个trans
	private String transPath = "";
	private String outPutPath = "";
	private String pathOutPath = "";
	private IntegerListGraphInputFormat graphInput;
	private ListTransInputFormat transInput;
	private ArrayList<ArrayList<Integer>> totalBase = new ArrayList<ArrayList<Integer>>();
	private ArrayList<ArrayList<Integer>> pathBase = new ArrayList<ArrayList<Integer>>();

	private ArrayList<String> sequenceBase = new ArrayList<String>();

	public GraphTotalIntegerTrans(IntegerListGraphInputFormat graphinput, ListTransInputFormat transinput) {
		this.graphInput = graphinput;
		this.transInput = transinput;
	}

	// 初始化，获得graph，transbase及各种映射表
	public void init() throws Exception {
		graphInput.setPath(graphPath);
		transInput.setItemspath(itemsPath);
		transInput.setTranspath(transPath);
		// System.out.println("xxxx"+graphinput.getPath());
		edgeSet = graphInput.graphInit();
		transBase = transInput.transInit();
		outDegree = graphInput.getDegree();
		vertexMap = graphInput.getVertexMap();
		vertexTrans = transInput.getTransMap();
		// return this.Graph;
	}


	public ArrayList<ArrayList<Integer>> getGraph() {
		return this.edgeSet;
	}

	public ArrayList<ArrayList<Integer>> getTransbase() {
		return this.transBase;
	}

	public ArrayList<ArrayList<Integer>> getTotalbase() {
		return this.totalBase;
	}

	public HashMap<Integer, Integer> getVertexMap() {
		return this.vertexMap;
	}

	public int getPathSize() {
		return pathSize;
	}

	public void setPathSize(int pathsize) {
		this.pathSize = pathsize;
	}

	public HashMap<Integer, Integer> getVertexTrans() {
		return this.vertexTrans;
	}

	public void setGraphPath(String GraphPath) {
		this.graphPath = GraphPath;
	}

	public String getGraphPath() {
		return graphPath;
	}

	public void setTransPath(String transpath) {
		this.transPath = transpath;
	}

	public String getTransPath() {
		return transPath;
	}

	public void setItemsPath(String itemspath) {
		this.itemsPath = itemspath;
	}

	public String getItemsPath() {
		return itemsPath;
	}

	public void setOutputPath(String outputpath) {
		this.outPutPath = outputpath;
	}

	public String getOutputPath() {
		return outPutPath;
	}

	public Integer getVertex() {
		return vertex;
	}

	public void setVertex(Integer vertex) {
		this.vertex = vertex;
	}

	public ArrayList<ArrayList<Integer>> getPathbase() {
		return pathBase;
	}

	public void setPathbase(ArrayList<ArrayList<Integer>> pathbase) {
		this.pathBase = pathbase;
	}

	public String getPathoutpath() {
		return pathOutPath;
	}

	public void setPathoutpath(String pathoutpath) {
		this.pathOutPath = pathoutpath;
	}

	public ArrayList<String> getSequenceBase() {
		return sequenceBase;
	}

	public void setSequenceBase(ArrayList<String> sequenceBase) {
		this.sequenceBase = sequenceBase;
	}

	public void setTotalbase(ArrayList<ArrayList<Integer>> totalbase) {
		this.totalBase = totalbase;
	}

	public void getAllpath() {

		ArrayList<ArrayList<Integer>> paths = new ArrayList<ArrayList<Integer>>();
		Queue<Integer> q = new LinkedList<>();
		q.add(this.vertex);
		int pathnum = 0;
		while (!q.isEmpty()) {

			if (paths.size() == 0) {
				Integer top = q.poll();
				ArrayList<Integer> edge = edgeSet.get(vertexMap.get(top).intValue());
				for (int i = 0; i < edge.size(); i++) {
					ArrayList<Integer> trans_seq = new ArrayList<Integer>();
					q.add(edge.get(i));
					trans_seq.add(top);
					trans_seq.add(edge.get(i));
					paths.add(trans_seq);
				}
				pathnum = 1;
			} else {
				for (int i = 0; i < paths.size(); i++) {
					Integer top = q.poll();
					ArrayList<Integer> path = paths.get(i);
					pathnum = path.size();
					if (pathnum >= this.pathSize + 1) {
						q.clear();
						break;
					} else {
						if (vertexMap.get(top) != null) {
							ArrayList<Integer> edge = edgeSet.get(vertexMap.get(top).intValue());
							if (edge.size() != 0) {
								for (int j = 0; j < edge.size(); j++) {
									ArrayList<Integer> onepath = new ArrayList<Integer>();
									for (int k = 0; k < path.size(); k++) {
										onepath.add(path.get(k));
									}
									// if(!onepath.contains(edge.get(j))){
									q.add(edge.get(j));
									onepath.add(edge.get(j));
									paths.add(onepath);
									// }
								}
							}
						}
					}
					paths.remove(i);
					i = i - 1;
				}
			}
		}
		for (int i = 0; i < paths.size(); i++) {
			this.pathBase.add(paths.get(i));
		}
	}

	public void getAllTrans() throws Exception {
		Object[] TransToItems = ListTransInputFormat.reader(itemsPath);
		for (int i = 0; i < pathBase.size(); i++) {
			ArrayList<ArrayList<Integer>> transset = new ArrayList<ArrayList<Integer>>();
			ArrayList<Integer> path = pathBase.get(i);
			for (int j = 0; j < path.size(); j++) {
				if (j == 0) {
					ArrayList<Integer> onetrans = new ArrayList<Integer>();
					if (this.vertexTrans.get(path.get(j)) != null) {
						onetrans = transBase.get(this.vertexTrans.get(path.get(j)).intValue());
						for (int k = 0; k < onetrans.size(); k++) {
							ArrayList<Integer> newtrans = new ArrayList<Integer>();
							if (!onetrans.get(k).equals("")) {
								newtrans.add(onetrans.get(k));
								transset.add(newtrans);
							}
						}
					}
				} else {
					for (int k = 0; k < transset.size(); k++) {
						ArrayList<Integer> onetrans = transset.get(k);
						if (onetrans.size() == j + 1) {
							break;
						} else {
							if (onetrans.size() != 0) {
								if (this.vertexTrans.get(path.get(j)) != null) {
									ArrayList<Integer> jtrans = transBase
											.get(this.vertexTrans.get(path.get(j)).intValue());
									// ArrayList<String>
									// jtrans=transbase.get(this.vertextrans.get(2).intValue());
									for (int m = 0; m < jtrans.size(); m++) {
										if (!jtrans.get(m).equals("")) {
											ArrayList<Integer> newtrans = new ArrayList<Integer>();
											for (int n = 0; n < onetrans.size(); n++) {
												newtrans.add(onetrans.get(n));
											}
											newtrans.add(jtrans.get(m));
											transset.add(newtrans);
										}
									}
								}
							}
						}
						transset.remove(k);
						k = k - 1;
					}
				}
			}
			for (int j = 0; j < transset.size(); j++) {
				ArrayList<Integer> onetrans = new ArrayList<Integer>();
				onetrans = transset.get(j);
				this.totalBase.add(onetrans);
				String s = "";
				for (int m = 0; m < onetrans.size(); m++) {
					s = s + TransToItems[onetrans.get(m)] + "\t" + "-1" + "\t";
				}
				s = s.substring(0, s.length() - 4);
				sequenceBase.add(s);
			}
		}

	}

	public void sequenceWriter() throws Exception {
		FileOutputStream out = new FileOutputStream(this.outPutPath);
		OutputStreamWriter osw = new OutputStreamWriter(out, "UTF8");
		BufferedWriter bwwrite = new BufferedWriter(osw);
		Object[] TransToItems = ListTransInputFormat.reader(itemsPath);
		if (this.totalBase.size() != 0) {
			for (int i = 0; i < this.totalBase.size(); i++) {
				ArrayList<Integer> one_seq = new ArrayList<Integer>();
				one_seq = this.totalBase.get(i);
				String s = "";
				for (int j = 0; j < one_seq.size(); j++) {
					s = s + TransToItems[one_seq.get(j)] + "\t" + "-1" + "\t";
					// s=s+one_seq.get(j)+"\t"+"-1"+"\t";
				}
				s = s.substring(0, s.length() - 4);
				bwwrite.write(s);
				bwwrite.newLine();
			}
		}
		bwwrite.close();
		osw.close();
		out.close();
	}

	public void pathWriter() throws Exception {
		FileOutputStream out = new FileOutputStream(this.pathOutPath);
		OutputStreamWriter osw = new OutputStreamWriter(out, "UTF8");
		BufferedWriter bwwrite = new BufferedWriter(osw);
		if (this.pathBase.size() != 0) {
			for (int i = 0; i < this.pathBase.size(); i++) {
				ArrayList<Integer> one_seq = new ArrayList<Integer>();
				one_seq = this.pathBase.get(i);
				String s = "";
				for (int j = 0; j < one_seq.size(); j++) {
					s = s + one_seq.get(j) + "\t" + "-1" + "\t";
				}
				s = s.substring(0, s.length() - 4);
				bwwrite.write(s);
				bwwrite.newLine();
			}
		}
		bwwrite.close();
		osw.close();
		out.close();
	}

}
