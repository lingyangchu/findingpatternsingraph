package com.bupt.mtl.patterns;

/**
 * A class for sequential patterns.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class Pattern {
	public String pattern = "";
	public Integer num = 0;

	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public int compare(Pattern o1, Pattern o2) {
		return (o1.num.compareTo(o2.num));
	}

}
