package com.bupt.mtl.patterns;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/**
 * PrefixSpan with no pattern weights, implemented by hashmap.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class SequenceMining {
	// all sequences
	private ArrayList<String> sequenceBase = new ArrayList<String>();
	// minimal support
	private int min_sup;
	// frequent patterns
	private HashMap<String, Integer> frePatterns = new HashMap<String, Integer>();
	// single items
	private ArrayList<HashSet<String>> singleItems = new ArrayList<HashSet<String>>();
	// read file from filepath
	private String filePath = "";
	//top-k patterns
	private HashMap<String, Integer> sequentialPattern = new HashMap<String, Integer>();
	//top-k
	int k = 0;
	//path size
	private int pathSize = 0;
	private String frePaPath = "";
	//items which is frequent in one jump
	private ArrayList<HashMap<String, Integer>> singleFre = new ArrayList<HashMap<String, Integer>>();
	//items which is frequent in every jump
	private HashSet<String> globalItems = new HashSet<String>();

	public ArrayList<String> getSequenceBase() {
		return sequenceBase;
	}
	public void setSequenceBase(ArrayList<String> sequenceBase) {
		this.sequenceBase = sequenceBase;
	}
	public int getMin_sup() {
		return min_sup;
	}
	public void setMin_sup(int min_sup) {
		this.min_sup = min_sup;
	}
	public HashMap<String, Integer> getFrePatterns() {
		return frePatterns;
	}
	public void setFrePatterns(HashMap<String, Integer> frePatterns) {
		this.frePatterns = frePatterns;
	}
	public ArrayList<HashSet<String>> getSingleItems() {
		return singleItems;
	}
	public void setSingleItems(ArrayList<HashSet<String>> singleItems) {
		this.singleItems = singleItems;
	}
	public String getFilepath() {
		return filePath;
	}
	public void setFilepath(String filepath) {
		this.filePath = filepath;
	}
	public HashMap<String, Integer> getSequentialPattern() {
		return sequentialPattern;
	}
	public void setSequentialPattern(HashMap<String, Integer> sequentialPattern) {
		this.sequentialPattern = sequentialPattern;
	}
	public int getK() {
		return k;
	}
	public void setK(int k) {
		this.k = k;
	}
	public int getPathsize() {
		return pathSize;
	}
	public void setPathsize(int pathsize) {
		this.pathSize = pathsize;
	}
	public String getFrePaPath() {
		return frePaPath;
	}
	public void setFrePaPath(String frePaPath) {
		this.frePaPath = frePaPath;
	}
	public ArrayList<HashMap<String, Integer>> getSingleFre() {
		return singleFre;
	}
	public void setSingleFre(ArrayList<HashMap<String, Integer>> singleFre) {
		this.singleFre = singleFre;
	}
	public HashSet<String> getGlobalItems() {
		return globalItems;
	}
	public void setGlobalItems(HashSet<String> globalItems) {
		this.globalItems = globalItems;
	}
	/** Read transactions from files */
	public void sequenceReader() throws Exception {
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(filePath);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				sequenceBase.add(line);
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
	}

	/** whether a item is frequent in a sequence */
	public int isFrequent(String item, ArrayList<Position> suffixList) {
		int count = 0;
		for (int i = 0; i < suffixList.size(); i++) {
			Position frePos = new Position();
			frePos = suffixList.get(i);
			int seqNum = suffixList.get(i).getSeqNum();
			int paPos = suffixList.get(i).getPaPos();
			String[] suffixStr = sequenceBase.get(seqNum).split("\t");
			for (int j = paPos + 1; j < suffixStr.length; j++) {
				if (suffixStr[j].equals(item)) {
					count = count + 1;
					break;
				}
			}
		}
		return count;
	}

	/** get single item which frequent */
	@SuppressWarnings("unchecked")
	public void getFrequentSingles() {
		for (int i = 0; i < sequenceBase.size(); i++) {
			String[] singleTrans = null;
			String singleSeq = "";
			singleSeq = sequenceBase.get(i);
			//transactions are split by \t-1\t
			singleTrans = singleSeq.split("\t-1\t");
			for (int j = 0; j < singleTrans.length; j++) {
				String[] items = null;
				items = singleTrans[j].split("\t");
				HashSet<String> tempItems = new HashSet<String>();
				HashMap<String, Integer> tempFre = new HashMap<String, Integer>();
				if (i == 0) {
					for (int m = 0; m < items.length; m++) {
						if (!tempItems.contains(items[m])) {
							tempItems.add(items[m]);
							tempFre.put(items[m], 1);
						}
					}
					singleItems.add(tempItems);
					singleFre.add(tempFre);
				} else {
					tempItems = singleItems.get(j);
					tempFre = singleFre.get(j);

					for (String item : tempFre.keySet()) {
						for (int m = 0; m < items.length; m++) {
							if (item.equals(items[m])) {
								tempFre.put(item, tempFre.get(item) + 1);
								break;
							}
						}
					}

					for (int m = 0; m < items.length; m++) {
						if (!tempItems.contains(items[m])) {
							tempItems.add(items[m]);
							tempFre.put(items[m], 1);
						}
					}

					singleItems.set(j, tempItems);
					singleFre.set(j, tempFre);
				}
			}
		}
		for (int j = 0; j < singleItems.size(); j++) {
			HashSet<String> tempItems = singleItems.get(j);
			for (String item : tempItems) {
				if (!globalItems.contains(item)) {
					globalItems.add(item);
				}
			}
		}
		globalItems.add("-1");
		if (min_sup > 1) {
			for (int i = 0; i < singleFre.size(); i++) {
				HashSet<String> sItems = singleItems.get(i);
				HashMap<String, Integer> sFre = singleFre.get(i);
				for (String item : sFre.keySet()) {
					if (sFre.get(item) < min_sup) {
						sItems.remove(item);
					}
				}
				singleItems.set(i, sItems);
			}
			globalItems.clear();
			for (int j = 0; j < singleItems.size(); j++) {
				HashSet<String> tempItems = singleItems.get(j);
				for (String item : tempItems) {
					if (!globalItems.contains(item)) {
						globalItems.add(item);
					}
				}
			}
			globalItems.add("-1");
			for (int i = 0; i < sequenceBase.size(); i++) {
				String[] singleTrans = null;
				String singleSeq = "";
				singleSeq = sequenceBase.get(i);
				String newSeq = "";
				singleTrans = singleSeq.split("\t-1\t");
				int flag = 0;
				for (int j = 0; j < singleTrans.length; j++) {
					HashSet<String> sItems = singleItems.get(j);
					String newTrans = "";
					String[] items = null;
					items = singleTrans[j].split("\t");
					for (int m = 0; m < items.length; m++) {
						if (sItems.contains(items[m])) {
							newTrans = newTrans + items[m] + "\t";
						}
					}

					if (newTrans.length() == 0) {
						flag = 1;
						break;
					} else {
						newTrans = newTrans.substring(0, newTrans.length() - 1);
						newSeq = newSeq + newTrans + "\t-1\t";
					}
				}
				if (flag == 1) {
					sequenceBase.remove(i);
					i = i - 1;
				} else {
					newSeq = newSeq.substring(0, newSeq.length() - 4);
					sequenceBase.set(i, newSeq);
				}
			}
		}
	}

	/** Prefix Pattern Mining */
	public HashMap<String, ArrayList<Position>> searchFrequentPatterns(String prefixStr,
			ArrayList<Position> suffixList) {
		HashMap<String, ArrayList<Position>> suffixMap = new HashMap<String, ArrayList<Position>>();
		for (String singleItem : globalItems) {
			ArrayList<Position> newSuffixList = new ArrayList<Position>();
			int count = 0;
			int pCount = 0;
			String[] prefixItems = prefixStr.split("\t");
			if (!(prefixItems[prefixItems.length - 1].equals("-1") && singleItem.equals("-1"))) {
				String newPrefix = prefixStr + "\t" + singleItem;
				String[] newPrefixItems = newPrefix.split("\t");
				for (int n = 0; n < newPrefixItems.length; n++) {
					if (newPrefixItems[n].equals("-1")) {
						pCount = pCount + 1;
					}
				}

				for (int i = 0; i < suffixList.size(); i++) {
					Position sufPos = new Position();
					int seqNum = suffixList.get(i).getSeqNum();
					int paPos = suffixList.get(i).getPaPos();
					String suffixStr = sequenceBase.get(seqNum);
					String[] items = suffixStr.split("\t");
					String newSuffixStr = "";
					for (int j = paPos + 1; j < items.length; j++) {
						if (items[j].equals(singleItem)) {
							for (int m = j + 1; m < items.length; m++) {
								newSuffixStr = newSuffixStr + items[m] + "\t";
							}
							sufPos.setSeqNum(seqNum);
							sufPos.setPaPos(j);
							count = count + 1;
							break;
						}
					}
					if (newSuffixStr.length() != 0) {
						int sCount = 0;
						String[] suffixItems = newSuffixStr.split("\t");
						for (int m = 0; m < suffixItems.length; m++) {
							if (suffixItems[m].equals("-1")) {
								sCount = sCount + 1;
							}
						}
						if (sCount + pCount == this.pathSize) {
							newSuffixList.add(sufPos);
						}
					}
				}
			}
			if (count >= min_sup) {
				if (pCount == this.pathSize && !singleItem.equals("-1")) {
					frePatterns.put(prefixStr + "\t" + singleItem, count);
				}
			}
			if (newSuffixList.size() >= min_sup) {
				suffixMap.put(prefixStr + "\t" + singleItem, newSuffixList);

			}
		}
		return suffixMap;
	}
	
	@SuppressWarnings("unchecked")
	/** calculate prefix pattern */
	public void prefixSpanCalculate() {
		HashMap<String, ArrayList<Position>> firstMap = new HashMap<String, ArrayList<Position>>();
		HashMap<String, ArrayList<Position>> secondMap = new HashMap<String, ArrayList<Position>>();
		getFrequentSingles();
		for (String singleItem : globalItems) {
			ArrayList<Position> newSuffixList = new ArrayList<Position>();
			if (!singleItem.equals("-1")) {
				for (int i = 0; i < sequenceBase.size(); i++) {
					String suffixStr = sequenceBase.get(i);
					String[] items = suffixStr.split("\t");
					String newSuffixStr = "";
					Position suPos = new Position();
					for (int j = 0; j < items.length; j++) {
						if (items[j].equals(singleItem)) {
							if (j != items.length - 1) {
								for (int k = j + 1; k < items.length; k++) {
									newSuffixStr = newSuffixStr + items[k] + "\t";
								}
								suPos.setSeqNum(i);
								suPos.setPaPos(j);
							}
							break;
						}
					}
					if (newSuffixStr.length() != 0) {
						newSuffixStr = newSuffixStr.substring(0, newSuffixStr.length() - 1);
						int suffixCount = 0;
						String[] suffixItems = newSuffixStr.split("\t");
						for (int m = 0; m < suffixItems.length; m++) {
							if (suffixItems[m].equals("-1")) {
								suffixCount = suffixCount + 1;
							}
						}
						if (suffixCount == this.pathSize) {
							newSuffixList.add(suPos);
						}
					}
				}
			}
			if (newSuffixList.size() != 0) {
				firstMap.put(singleItem, newSuffixList);
			}
		}
		PatternHeap heap = new PatternHeap(k);
		while (firstMap.size() != 0) {
			for (String key : firstMap.keySet()) {
				HashMap<String, ArrayList<Position>> tempMap = new HashMap<String, ArrayList<Position>>();
				String[] patternItems = key.split("\t");
				int pCount = 0;
				for (int n = 0; n < patternItems.length; n++) {
					if (patternItems[n].equals("-1")) {
						pCount = pCount + 1;
					}
				}
				if (pCount != pathSize) {
					tempMap = searchFrequentPatterns(key, firstMap.get(key));
					for (String tempkey : tempMap.keySet()) {
						secondMap.put(tempkey, tempMap.get(tempkey));
					}
					tempMap.clear();
				} else {
					if (!(patternItems[patternItems.length - 1].equals("-1"))) {
						HashMap<String, Integer> endMap = new HashMap<String, Integer>();
						heap.topKPatterns(frePatterns);
						endMap = heap.topkMap(heap.queue);
						frePatterns.clear();
						frePatterns = (HashMap<String, Integer>) endMap.clone();
						if (frePatterns.containsKey(key)) {
							tempMap = searchFrequentPatterns(key, firstMap.get(key));
							for (String tempkey : tempMap.keySet()) {
								secondMap.put(tempkey, tempMap.get(tempkey));
							}
							tempMap.clear();
						}
					} else {
						tempMap = searchFrequentPatterns(key, firstMap.get(key));
						for (String tempkey : tempMap.keySet()) {
							secondMap.put(tempkey, tempMap.get(tempkey));
						}
						tempMap.clear();
					}
				}
			}
			firstMap.clear();
			firstMap = (HashMap<String, ArrayList<Position>>) secondMap.clone();
			secondMap.clear();
		}
		if (firstMap.size() == 0) {
			HashMap<String, Integer> endMap = new HashMap<String, Integer>();
			heap.topKPatterns(frePatterns);
			endMap = heap.topkMap(heap.queue);
			frePatterns.clear();
			frePatterns = (HashMap<String, Integer>) endMap.clone();
		}
	}

	/** get top-k patterns */
	public void miningSequentialPatterns() throws Exception {
		FrequencyHeap endHeap = new FrequencyHeap(k);
		HashMap<String, Integer> endMap = new HashMap<String, Integer>();
		endHeap.topKFrequency(frePatterns);
		sequentialPattern = endHeap.topkMap(endHeap.queue);
	}
	/** write patterns */
	public void patternWriter() throws Exception {
		FileOutputStream out = new FileOutputStream(frePaPath);
		OutputStreamWriter osw = new OutputStreamWriter(out, "UTF8");
		BufferedWriter bwwrite = new BufferedWriter(osw);
		if (frePatterns.size() != 0) {
			for (String pa : frePatterns.keySet()) {
				bwwrite.write(pa + " " + frePatterns.get(pa));
				bwwrite.newLine();
			}
		}
		bwwrite.close();
		osw.close();
		out.close();
	}

	/** Read patterns from files */
	public void patternReader() throws Exception {
		FileInputStream inputStream = null;
		Scanner sc = null;
		try {
			inputStream = new FileInputStream(frePaPath);
			sc = new Scanner(inputStream, "UTF-8");
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] paFre = line.split(" ");
				frePatterns.put(paFre[0], Integer.valueOf(paFre[1]));
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (sc != null) {
				sc.close();
			}
		}
	}

}