package com.bupt.mtl.patterns;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
/**
 * a max heap for the frequencies of patterns.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */

//
public class FrequencyHeap<E extends Comparable> {
	public Object pattern;
	public PriorityQueue<Pattern> queue;
	public int k; 
	public int equalNum = 0;
	public HashMap<Integer, Integer> equalMap = new HashMap<Integer, Integer>();

	public FrequencyHeap(int k) {
		if (k <= 0)
			throw new IllegalArgumentException();
		this.k = k;
		this.queue = new PriorityQueue(k, new Comparator<Pattern>() {
			public int compare(Pattern o1, Pattern o2) {
				return (o1.num.compareTo(o2.num));
			}
		});
	}

	public void topKFrequency(HashMap<String, Integer> patterns) {
		for (String key : patterns.keySet()) {
			Pattern p = new Pattern();
			p.setPattern(key);
			p.setNum(patterns.get(key));
			if (queue.size() < k) {
				queue.add(p);
			} else {
				if (queue.peek().num.intValue() < p.getNum().intValue()) {
					queue.poll();
					queue.add(p);
				}
			}
		}
	}

	public HashMap<String, Integer> topkMap(PriorityQueue queue) {
		HashMap<String, Integer> patterns = new HashMap<String, Integer>();
		while (!queue.isEmpty()) {
			Pattern p = new Pattern();
			p = (Pattern) queue.poll();
			if (patterns.containsKey(p.getPattern())) {
				patterns.put(p.getPattern(), patterns.get(p.getPattern()) + p.getNum());
			} else {
				patterns.put(p.getPattern(), p.getNum());
			}
		}
		return patterns;
	}

	public void patternWriter(String output, HashMap<String, Integer> map, int k) throws Exception {
		FileOutputStream out = new FileOutputStream(output);
		OutputStreamWriter osw = new OutputStreamWriter(out, "UTF8");
		BufferedWriter bwWrite = new BufferedWriter(osw);
		ArrayList<Integer> freq = new ArrayList<Integer>();
		if (map.size() != 0) {
			FrequencyHeap sampledOld = new FrequencyHeap(k);
			sampledOld.topKFrequency(map);
			while (!sampledOld.queue.isEmpty()) {
				Pattern p = new Pattern();
				p = (Pattern) sampledOld.queue.poll();
				p.getPattern();
				freq.add(p.getNum());
			}
			Collections.sort(freq);
			Collections.reverse(freq);
			for (int i = 0; i < freq.size(); i++) {
				bwWrite.write(freq.get(i).toString());
				bwWrite.newLine();
			}
		}
		bwWrite.close();
		osw.close();
		out.close();
	}
}