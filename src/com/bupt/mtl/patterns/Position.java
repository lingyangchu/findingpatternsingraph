package com.bupt.mtl.patterns;

/**
 * projection databases.
 * @author Mingtao Lei
 * @Time 2016-03-08
 */
public class Position {

	public int seqNum = 0;
	public int paPos = 0;

	public int getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(int seqNum) {
		this.seqNum = seqNum;
	}

	public int getPaPos() {
		return paPos;
	}

	public void setPaPos(int paPos) {
		this.paPos = paPos;
	}

}
