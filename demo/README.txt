1. What' s this?
These codes are made for the experiments of the paper "Patterns in directed graph". For more details, please read our paper.
2. What' the meaning of "graph.txt","trans.txt" and "items.txt".
graph.txt is the file of the so-called directed graph, each line is an edge with a format of "vertexId\tvertexId", for example, an edge (4,6) is donated by "4\t6".
trans.txt is the file of transactions of each vertex. Each line ia a mapping from vertexId to transactions. For example, vertex 4 has a transaction whose id is 5, then it is donated by "4\t5". If two transactions have different ids but have same itemsets, we treat them as the same transaction for save the space.
items.txt is the file of itemsets of transactions. Each line is an itemset. For example, if a transaction has an itemset of (4,5,6,7), that line will be "4\t\5\t6\t7" and the transaction-id will be the id of that line.
3. How about the running environment of codes?
The codes were made by java. Please use it in Java 1.8 or higher version.
4. How to use the codes?
Open the code in eclipse or other IDEs. Find a java file named "MineSequenceWithSampling.java" and set the parameters "k", "sampleSize", "graphPath", "transPath", "itemsPath", "pathSize" and "min_sup". Then, run it till it ends. For more details, please read our codes and comments.
For just running a demo, one can just run this file with defalt parameters.
5. How about the result?
Please look forward to the doc named "patterns.txt" under "demo/result". The demo result is the top-5 patterns in descend order of its supports.